<?php

declare(strict_types=1);

namespace Drupal\drowl_project_settings\Form;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Drowl project settings settings for this site.
 */
class DrowlProjectSettingsInformationForm extends ConfigFormBase {

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a new DrowlProjectSettingsInformationForm object.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   */
  public function __construct(ModuleHandlerInterface $module_handler) {
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'drowl_project_settings_project_information';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['drowl_project_settings.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    // Show token browser for easier lookup:
    if ($this->moduleHandler->moduleExists('token')) {
      $form['tokens']['token_browser'] = [
        '#theme' => 'token_tree_link',
        '#token_types' => ['project'],
      ];
    }

    $form['contact'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Contact Information'),
    ];
    $form['contact']['address'] = [
      '#type' => 'address',
      '#title' => $this->t('Address'),
      '#required' => TRUE,
      '#default_value' => $this->config('drowl_project_settings.settings')->get('address'),
      '#field_overrides' => [
        'givenName' => 'optional',
        'additionalName' => 'optional',
        'familyName' => 'optional',
        'organization' => 'optional',
        'addressLine1' => 'required',
        'addressLine2' => 'optional',
        'postalCode' => 'required',
        'sortingCode' => 'hidden',
        'dependentLocality' => 'hidden',
        'locality' => 'required',
        'administrativeArea' => 'hidden',
      ],
    ];
    $form['contact']['geo_coordinates'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Geo-Coordinates'),
      '#description' => $this->t('Geo-Coordinates in the format <i>52.25739,8.93206</i> (no spaces!).'),
      '#default_value' => $this->config('drowl_project_settings.settings')->get('geo_coordinates'),
    ];
    $form['contact']['mail'] = [
      '#type' => 'email',
      '#title' => $this->t('Email'),
      // There are cases where it's NOT deserved as required field:
      '#required' => FALSE,
      '#description' => $this->t('The public mail address of the company, e.g. <i>info@company.com</i>. Typically this value is deserved for legal reasons, but there are edge-cases where this might be optional.'),
      '#default_value' => $this->config('drowl_project_settings.settings')->get('mail'),
    ];
    $form['contact']['tel'] = [
      '#type' => 'tel',
      '#title' => $this->t('Telephone'),
      // There are cases where it's NOT deserved as required field:
      '#required' => FALSE,
      '#description' => $this->t('Specify the phone number in the format <b><code>+CCC AAA XXXXXX</code></b> where <b><code>+CCC</code></b> is the country code, <b><code>AAA</code></b> the area code and <b><code>XXXXXX</code></b> the local number <i>(e.g. <code>+49 1234 567890</code>)</i>. Typically this value is deserved for legal reasons, but there are edge-cases where this might be optional.'),
      '#default_value' => $this->config('drowl_project_settings.settings')->get('tel'),
    ];
    $form['contact']['fax'] = [
      '#type' => 'tel',
      '#title' => $this->t('Fax'),
      '#description' => $this->t('Please specify the fax number in the format <b><code>+CCC AAA XXXXXX</code></b> where <b><code>+CCC</code></b> is the country code, <b><code>AAA</code></b> the area code and <b><code>XXXXXX</code></b> the local number <i>(e.g. <code>+49 1234 567890</code>)</i>.'),
      '#default_value' => $this->config('drowl_project_settings.settings')->get('fax'),
    ];
    $form['contact']['vat_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('VAT ID'),
      // There are cases where it's NOT deserved as required field:
      '#required' => FALSE,
      '#default_value' => $this->config('drowl_project_settings.settings')->get('vat_id'),
    ];
    $form['contact']['company_law'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Legal information about the company'),
      '#description' => $this->t('The legal information about the company (e.g. for the imprint).<br><b>Which information is obligatory depends on your legal form.</b>'),
      '#required' => TRUE,
      '#default_value' => $this->config('drowl_project_settings.settings')->get('company_law')['value'],
      '#format' => $this->config('drowl_project_settings.settings')->get('company_law')['format'],
    ];
    $form['contact']['project_owner'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Project Owner'),
      '#description' => $this->t('Name and address of the project owner in one line, e.g. for metadata.'),
      // There are cases where it's NOT deserved as required field:
      '#required' => FALSE,
      '#default_value' => $this->config('drowl_project_settings.settings')->get('project_owner'),
    ];

    $form['description'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Description'),
    ];
    $form['description']['description_short'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Short Description'),
      '#description' => $this->t('A short description of the project ("Elevator pitch") in 2-3 sentences utilizing the most important keywords.<br>Can be used e.g. as the meta-description of the main page.'),
      '#required' => TRUE,
      '#default_value' => $this->config('drowl_project_settings.settings')->get('description_short'),
    ];

    $form['copyright'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Copyrights'),
    ];
    $form['copyright']['copyrights'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Copyrights'),
      '#required' => TRUE,
      '#default_value' => $this->config('drowl_project_settings.settings')->get('copyrights')['value'],
      '#format' => $this->config('drowl_project_settings.settings')->get('copyrights')['format'],
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('drowl_project_settings.settings')
      ->set('address', $form_state->getValue('address'))
      ->set('geo_coordinates', $form_state->getValue('geo_coordinates'))
      ->set('mail', $form_state->getValue('mail'))
      ->set('tel', $form_state->getValue('tel'))
      ->set('fax', $form_state->getValue('fax'))
      ->set('vat_id', $form_state->getValue('vat_id'))
      ->set('company_law', $form_state->getValue('company_law'))
      ->set('project_owner', $form_state->getValue('project_owner'))
      ->set('description_short', $form_state->getValue('description_short'))
      ->set('copyrights', $form_state->getValue('copyrights'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
